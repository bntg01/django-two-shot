from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.filter(purchaser=request.user).values()
    expense_dict = {}
    if not expenses:
        expense_dict["User"] = "has no categories"
        context = {
            "expense_dict": expense_dict,
        }
        return render(request, "receipts/category_list.html", context)
    for index, _ in enumerate(receipts):
        name = expenses[receipts[index]["category_id"] - 1]
        if name in expense_dict:
            expense_dict[name] += 1
        else:
            expense_dict[name] = 1
    for expense in expenses:
        if expense not in expense_dict:
            expense_dict[expense] = 0
    context = {
        "expense_list": expenses,
        "expense_dict": expense_dict,
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.filter(purchaser=request.user).values()
    account_dict = {}
    if not accounts:
        account_dict["User"] = ["has", "no accounts"]
        context = {
            "account_dict": account_dict,
        }
        return render(request, "receipts/account_list.html", context)
    for index, _ in enumerate(receipts):
        name = accounts[receipts[index]["account_id"] - 1]
        num = accounts[receipts[index]["account_id"] - 1].number
        if name in account_dict:
            account_dict[name][1] += 1
        else:
            account_dict[name] = [num, 1]
    for account in accounts:
        if account not in account_dict:
            account_dict[account] = [account.number, 0]
    context = {
        "account_dict": account_dict,
    }

    return render(request, "receipts/account_list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
